<?php


return [
	'loghelper_stdout' => [
		'driver' => 'monolog',
		'level' => env('LOG_HELPER_DEBUG_LEVEL', 'debug'),
		'handler' => \Monolog\Handler\StreamHandler::class,
		'tap' => [\GetProfit\LogHelper\Logging\MonologTap::class],
//		'formatter' => env('PROFIT_LOG_STD_OUT_LOG_FORMATTER'),
		'with' => [
			'stream' => 'php://stdout',
		],
	],
	'loghelper_files' => [
		'driver' => 'daily',
		'days' => env('LOG_HELPER_FILES_CHANNEL_DAYS', 10),
		'level' => env('LOG_HELPER_DEBUG_LEVEL', 'debug'),
		'tap' => [\GetProfit\LogHelper\Logging\MonologTap::class],
//		'formatter' => env('PROFIT_LOG_STD_OUT_LOG_FORMATTER'),
		'path' => env('LOG_HELPER_FILES_CHANNEL_PATH', storage_path('logs/'.config('app.name').'.log'))
	],
];