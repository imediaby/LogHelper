## О пакете
Пакет призван упростить настройку логирования в Вашем laravel-микросервисе.
Несёт в себе конфигурацию канала и ряд вспомогательных сущностей для стандартизации реализации логирования в сервисах.
Логи пишутся в stdout.

## Установка
Поскольку репозиторий приватный, его нужно добавить как источник пакетов в `composer.json` проекта
```json
{
"..."
"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:imediasolutionsby/getprofit-loghelper.git"
        }
    ]
}
```
Дальнейшая установка производится командой
```
composer require getprofit/loghelper
```
## Использование
В настройках (или в env) вашего сервиса нужно указать использование канала логирования `loghelper_stdout` или `loghelper_files`  
### RequestId
В пакет входит `tap` для `Monolog`-каналов пакета, который добавляет в лог `RequestId`. Настроен и работает автоматически, никаких действий не требуется: просто используйте фасад `Log` как обычно.
### HTTP Middleware
В пакете поставляется глобальный Middleware для логирования всех HTTP запросов. Работает автоматически, никаких действий не требуется.
### Controller
Для логирования действий контроллера (start, finish) в классе Вашего `Controller` можно использовать трейт `\GetProfit\LogHelper\Http\Controllers\LoggableControllerTrait`.
Чтобы действие начало попадать в лог, его нужно указать в свойстве `loggableMethods` класса контроллера. 
```php
public array $loggableMethods = ['list'];
```
### Command 
Для логирования процесса выполнения `Command` (start, finish) в классе Вашего `Command` можно использовать трейт `\GetProfit\LogHelper\Command\LoggableCommandTrait`.
В лог добавляется название класса `Command`, а так же опции и аргументы вызова.
### Event 
В пакет входит Wildcard listener, который добавляет запись в лог при наступлении события.
Чтобы ваше событие попадало в лог, нужно использовать в классе события трейт `\GetProfit\LogHelper\Events\LoggableEventTrait` 
### Listener
Для логирования процесса выполнения `Listener` (start, finish) в классе Вашего `Listener` можно использовать трейт `\GetProfit\LogHelper\Listeners\LoggableListenerTrait`.
Логику вашего `Listener` в таком случае следует реализовывать в методе `handleAction` (вместо метода `handle`).
### Job 
#### Trait
Для логирования процесса выполнения `Job` (start, finish) в классе Вашего `Job` можно использовать трейт `\GetProfit\LogHelper\Jobs\LoggableJobTrait`.
Трейт реализует метод `middleware()`, поэтому если Ваш `Job` использует и другие `middleware` учитывайте это.
В лог добавляется объект `Job`, поэтому поля класса вашего `Job` лучше делать публичными (чтобы увидеть их в логе).
В лог добавляется так же результат работы `Job`, поэтому если хотите туда что-то добавить, можно из метода `handle` возвращать данные для добавления в лог.
#### Middleware
Вместо подключения трейта можно использовать middleware напрямую: в пакете поставляется `Middleware` для `Job`. Для использования нужно подключить `middleware` к Вашему `Job`.
Подробнее см. [документацию](https://laravel.com/docs/8.x/queues#job-middleware).
Поведение будет идентично трейту.
```php
use GetProfit\LogHelper\Jobs\Middleware\JobProcessLogMiddleware;

public function middleware()
{
    return [new JobProcessLogMiddleware];
}
```

### HTTP Client (Guzzle)
В пакете имеется Middleware `HttpClientMiddleware` для фасада `Http`.
По умолчанию включен и проброс заголовка, и логирование запроса-ответа. 
Для изменения поведения нужно воспользоваться методами `withRequestHeader(false)` и `withLogProcess(false)` соответственно.
(например, для запросов во внешние системы не имеет смысла пробрасывать наш внутренний загловок и следует его отключить).
```php
Http::asJson()->withMiddleware((new GetProfit\LogHelper\HttpClientMiddleware)->withRequestHeader(false));
```

## Конфигурация
Конфигурация осуществляется через `.env` файл. Доступны следующие настройки:

* `LOG_HELPER_REQUEST_ID_HEADER` - заголовок, в котором пробрасывается идентификатор запроса. По умолчанию `X-Request-Id`;
* `LOG_HELPER_DEBUG_LEVEL` - уровень логирования в stdout. По умолчанию `debug`;
* `LOG_HELPER_FILES_CHANNEL_DAYS` - устанавливает срок хранения файлов лога (при использовании файлового канала);
* `LOG_HELPER_FILES_CHANNEL_PATH` - устанавливает путь для хранения файлов лога (при использовании файлового канала).