<?php

namespace GetProfit\LogHelper\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\{Arr, Facades\Log};

class RequestLogMiddleware
{
    /**
     * @var array|string[]
     */
    private array $hiddenFields = [
        'password',
        'password_confirmation'
    ];


    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $data = [
            'app' => config('app.name'),
            'ip' => $request->ip(),
            'url' => $request->path(),
            'method' => $request->method(),
            'params' => [
                'get' => $this->replaceHiddenFields($request->query->all()),
                'post' => $this->replaceHiddenFields($request->request->all())
            ],
            'headers' => $request->headers->all(),
//            'body' => $request->getContent(), // turn off, cause personal data could be in broken body
        ];
	    Log::info('HTTP: request', $data);

	    $response = $next($request);

	    Log::info('HTTP: response', [
		    'status' => $response->status(),
		    'action' => $request->route() ? $request->route()->getActionName() : $request->route(),
		    'content' => $response->status() !== 200 ? $response->content() : ''
	    ]);


	    return $response;
    }

    /**
     * Replace hidden fields
     *
     * @param array $requestData
     * @return array
     */
    private function replaceHiddenFields(array $requestData): array
    {
        foreach ($this->hiddenFields as $field) {
            if (Arr::exists($requestData, $field)) {
                Arr::set($requestData, $field, '***');
            }
        }

        return $requestData;
    }
}
