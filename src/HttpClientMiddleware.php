<?php

namespace GetProfit\LogHelper;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Support\Facades\Log as LogFacade;
use GuzzleHttp\Exception\RequestException;


class HttpClientMiddleware
{
	protected bool $addRequestIdHeader = true;
	protected bool $logProcess = true;
	protected bool $logFailures = true;
	protected Log $log;

	public function __construct() {
		$this->log = app(Log::class);
	}

	public function withRequestHeader(bool $addHeader = true) {
		$this->addRequestIdHeader = $addHeader;
		return $this;
	}

	public function withLogProcess(bool $logProcess = true) {
		$this->logProcess = $logProcess;
		return $this;
	}

	public function withLogFailures(bool $logFailures = true) {
		$this->logFailures = $logFailures;
		return $this;
	}

	public function __invoke(callable $next)
	{
		return function (RequestInterface $request, array $options = []) use ($next) {
			if ($this->addRequestIdHeader === true)
				$request = $this->applyRequestIdHeader($request);

			if ($this->logProcess === true) {
				LogFacade::info('HTTP client: request', [
						'request' => $this->getRequestLogData($request),
						'options' => $options
					]
				);
			}

			return $next($request, $options)->then(
				$this->handleSuccess($request, $options),
				$this->handleFailure($request, $options)
			);
		};
	}

	protected function applyRequestIdHeader(RequestInterface $request)
	{
		$request = $request->withHeader($this->log->getRequestIdHeaderName(), $this->log->getRequestId());
		return $request;
	}

	protected function getRequestLogData(RequestInterface $request) : array {
		return [
			'method' => $request->getMethod(),
			'uri' => $request->getUri(),
			'body' => $request->getBody()->getContents(),
			'headers' => $request->getHeaders(),
		];
	}

	protected function getResponseLogData(ResponseInterface $response) : array {
		return [
			'status' => $response->getStatusCode(),
			'headers' => $response->getHeaders(),
			'body' => $response->getBody()->getContents(),
		];
	}

	private function handleSuccess(RequestInterface $request, array $options): callable
	{
		return function (ResponseInterface $response) use ($request, $options) {
			if ($this->logProcess === true) {
				LogFacade::info('HTTP client: response', [
						'response' => $this->getResponseLogData($response),
					]
				);
			}

			return $response;
		};
	}

	private function handleFailure(RequestInterface $request, array $options): callable
	{
		return function (\Exception $reason) use ($request, $options) {
			if ($this->logFailures === true) {
				LogFacade::error('HTTP client: failure', [
					'failureReasonResponse' => ($reason instanceof RequestException && $reason->hasResponse() === true) ? $reason->getResponse() : null,
					'reason' => $reason,
				]);
			}

			return \GuzzleHttp\Promise\rejection_for($reason);
		};
	}

}