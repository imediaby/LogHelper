<?php

namespace GetProfit\LogHelper;

use GetProfit\LogHelper\Providers\EventServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use GetProfit\LogHelper\Http\Middleware\RequestLogMiddleware;

class LogHelperServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/logging.php', 'logging.channels');
	    $this->app->register(EventServiceProvider::class);
    }

    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(RequestLogMiddleware::class);

        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('loghelper.php'),
            ], 'config');

        }
    }
}
