<?php

namespace GetProfit\LogHelper;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Psr\Log\LogLevel;

class Log
{
    protected static string $requestId = '';

    /**
     * LogService constructor.
     */

    public function __construct(Request $request)
    {
    	if (static::$requestId === '')
            static::$requestId = $request->header($this->getRequestIdHeaderName(), Str::uuid());
    }

    public function getRequestIdHeaderName() {
        return config('loghelper.request_id_header');
    }

    public function getRequestId() {
        return static::$requestId;
    }
}