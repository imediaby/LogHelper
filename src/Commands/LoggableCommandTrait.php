<?php


namespace GetProfit\LogHelper\Commands;

use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

trait LoggableCommandTrait {
	/**
	 * Run the console command.
	 *
	 * @param  \Symfony\Component\Console\Input\InputInterface  $input
	 * @param  \Symfony\Component\Console\Output\OutputInterface  $output
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		Log::info('Command: start', [
			'command' => get_class($this),
			'options' => $input->getOptions(),
			'arguments' => $input->getArguments(),
		]);

		$result = parent::execute($input, $output);

		Log::info('Command: finish', [
			'command' => get_class($this),
			'result' => $result
		]);

		return $result;
	}
}
