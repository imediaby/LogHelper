<?php
namespace GetProfit\LogHelper\Logging;

use GetProfit\LogHelper\Log;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;

class MonologTap
{
	protected ?Request $request;
	protected Log $log;

	public function __construct(Log $log, Request $request = null)
	{
		$this->request = $request;
		$this->log = $log;
	}

	public function __invoke(Logger $logger)
	{
		if ($this->request) {
			foreach ($logger->getHandlers() as $handler) {
				$handler->pushProcessor([$this, 'processLogRecord']);
			}
		}
	}

	public function processLogRecord(array $record): array
	{
		$record['context'] += [
			$this->log->getRequestIdHeaderName() => $this->log->getRequestId(),
		];

		return $record;
	}
}
