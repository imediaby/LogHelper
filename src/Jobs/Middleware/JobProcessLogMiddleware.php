<?php

namespace GetProfit\LogHelper\Jobs\Middleware;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Facades\Log;

class JobProcessLogMiddleware
{
    /**
     * Process the job.
     * @param  mixed  $job
     * @param  callable  $next
     * @return mixed
     */
    public function handle($job, $next)
    {
        Log::info('Job: start', [
	        'job' => get_class($job),
	        'data' => $job instanceof Arrayable ? $job->toArray() : $job,
        ]);

        $result = $next($job);

	    Log::info('Job: finish', [
		    'job' => get_class($job),
		    'result' => $result
	    ]);

        return $result;
    }
}
