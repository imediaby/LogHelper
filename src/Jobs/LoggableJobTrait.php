<?php


namespace GetProfit\LogHelper\Jobs;

trait LoggableJobTrait {

	public function middleware()
	{
		return [new Middleware\JobProcessLogMiddleware];
	}
}